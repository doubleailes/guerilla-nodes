
-- Utility functions

function string.split(str, delimiter)
    local result = { }
    local from  = 1
    local delim_from, delim_to = string.find( str, delimiter, from)

    while delim_from do
        table.insert( result, string.sub( str, from , delim_from-1))
        from = delim_to + 1
        delim_from, delim_to = string.find( str, delimiter, from)
    end

    table.insert(result, string.sub(str, from))
    return result
end

function table.slice(tbl, first, last, step)
    local sliced = {}

    for i = first or 1, last or #tbl, step or 1 do
        sliced[#sliced+1] = tbl[i]
    end

    return sliced
end


-- Try to get the light shader directory

-- Get from GN_PLUGINS_LIBRARY
local libdir = os.getenv('GN_PLUGINS_LIBRARY')

-- Or get from the current directory
if libdir == nil then
    local sps = string.split(debug.getinfo(1).short_src, "/")

    local enddir = table.slice(sps, nil, #sps-2, nil)
    table.insert(enddir, 'library')
    libdir = table.concat(enddir, "/")
end

local lightshaderdir = libdir .. "/lights/shaders/"

-- Register barn door gizmo
LightShader.Gizmos.barndoors = {}
LightShader.Gizmos.barndoors.name = "Barn Doors"
LightShader.Gizmos.barndoors.shader = lightshaderdir .. "barndoors_light_shader.gnode"

function LightShader.Gizmos.barndoors:creategizmos (tool, gizmos)

	local	vu, vd, hl, hr, vb, hb = self:getinput ("VAngleU"), self:getinput ("VAngleD"), self:getinput ("HAngleL"), self:getinput ("HAngleR"), self:getinput ("VBase"), self:getinput ("HBase"), self:getinput ("Color")

	if vu and vd and hl and hr and vb and hb then
		-- create the 3d objects (gizmos) for horizontal and vertical manipulation
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true,  false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true,  true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false,  true, true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value, nil, nil, nil, nil, nil, nil, self.GizmoColor))
		local	vdu, vdd, hdl, hdr, vdb, hdb = self:getinput ("VDeltaU"), self:getinput ("VDeltaD"), self:getinput ("HDeltaL"), self:getinput ("HDeltaR"), self:getinput ("VBaseDelta"), self:getinput ("HBaseDelta")

		if vdu and vdd and hdl and hdr and vdb and hdb then
			-- if V Delta Angle and H Delta Angle are present, create the 3d objects
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, self.GizmoColor))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, self.GizmoColor))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, true, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, self.GizmoColor))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false, false, false, vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, self.GizmoColor))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, true,  true, true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value,  vdb.Value, hdb.Value, self.GizmoColor))
			table.insert (gizmos, GizmoBarnDoorsCreate (tool, self, false,  true, true,  vu.Value, vd.Value, hl.Value, hr.Value, vb.Value, hb.Value,  vdu.Value, vdd.Value, hdl.Value, hdr.Value, vdb.Value, hdb.Value, self.GizmoColor))
		end
	end

end